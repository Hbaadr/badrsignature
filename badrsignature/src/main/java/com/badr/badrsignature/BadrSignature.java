package com.badr.badrsignature;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.atomic.AtomicInteger;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BadrSignature {

    static private Handler handler = new Handler();
    static private AtomicInteger mCounter = new AtomicInteger();
    static private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mCounter = new AtomicInteger();
        }
    };

    public static void set(final Context context, View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(mRunnable);
                handler.postDelayed(mRunnable, 1000);
                if (mCounter.incrementAndGet() == 7) {
                    SweetAlertDialog.DARK_STYLE = true;
                    SweetAlertDialog alertDialog = new SweetAlertDialog(context);
                    alertDialog.setTitleText("Développé par");
                    alertDialog.setContentText("HOURIMECHE Badr");
                    alertDialog.setContentTextSize(20);
                    alertDialog.setCancelable(false);
                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://ma.linkedin.com/in/hbaadr")));
                            sDialog.cancel();
                        }
                    });
                    alertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    alertDialog.show();

                    Button confirmButton = alertDialog.findViewById(R.id.confirm_button);
                    confirmButton.setText("Visiter LinkedIn");
                    confirmButton.setBackgroundColor(context.getResources().getColor(R.color.blue_mercedes));
                    Button cancelButton = alertDialog.findViewById(R.id.cancel_button);
                    cancelButton.setText("Annuler");
                    cancelButton.setBackgroundColor(context.getResources().getColor(R.color.red_mercedes));
                    cancelButton.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    public static void set(final Context context, View view, final int visitColor, final int cancelColor) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(mRunnable);
                handler.postDelayed(mRunnable, 1000);
                if (mCounter.incrementAndGet() == 7) {
                    SweetAlertDialog.DARK_STYLE = true;
                    SweetAlertDialog alertDialog = new SweetAlertDialog(context);
                    alertDialog.setTitleText("Développé par");
                    alertDialog.setContentText("HOURIMECHE Badr");
                    alertDialog.setContentTextSize(20);
                    alertDialog.setCancelable(false);
                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://ma.linkedin.com/in/hbaadr")));
                            sDialog.cancel();
                        }
                    });
                    alertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    alertDialog.show();

                    Button confirmButton = alertDialog.findViewById(R.id.confirm_button);
                    confirmButton.setText("Visiter LinkedIn");
                    confirmButton.setBackgroundColor(visitColor);
                    Button cancelButton = alertDialog.findViewById(R.id.cancel_button);
                    cancelButton.setText("Annuler");
                    cancelButton.setBackgroundColor(cancelColor);
                    cancelButton.setVisibility(View.VISIBLE);

                }
            }
        });
    }
}
